package com.gildedrose;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GildedRoseTest {

    @Test
    public void foo() {
        GildedRose products = createGildedRose("foo", 0, 0);
        products.updateQuality();

        assertEquals("foo", products.items[0].name);
    }

    @Test
    public void agedBrieDecreasesSellIn() {
        GildedRose products = createGildedRose(GildedRoseFactory.BRIE, 2, 0);
        products.updateQuality();

        assertEquals(1, getItemSellIn(products));
    }


    @Test
    public void agedBrieIncreasesQualityByOne() {
        GildedRose products = createGildedRose(GildedRoseFactory.BRIE, 2, 0);
        products.updateQuality();

        assertEquals(1, getItemQuality(products));
    }


    @Test
    public void agedBrieIncreasesQualityByDouble() {
        GildedRose products = createGildedRose(GildedRoseFactory.BRIE, -1, 2);
        products.updateQuality();

        assertEquals(-2, getItemSellIn(products));
        assertEquals(4, getItemQuality(products));
    }


    @Test
    public void agedBrieIncreasesQualityToMax() {
        GildedRose products = createGildedRose(GildedRoseFactory.BRIE, 1, 50);
        products.updateQuality();

        assertEquals(50, getItemQuality(products));
    }


    @Test
    public void backstageEntryDecreaseSellIn() {
        GildedRose products = createGildedRose(GildedRoseFactory.BACKSTAGE_PASSES_ITEM, 10, 1);
        products.updateQuality();

        assertEquals(9, getItemSellIn(products));
    }


    @Test
    public void backstageEntryIncreaseQualityBy10Days() {
        GildedRose products = createGildedRose(GildedRoseFactory.BACKSTAGE_PASSES_ITEM, 10, 1);
        products.updateQuality();

        assertEquals(3, getItemQuality(products));
    }


    @Test
    public void backstageEntryIncreaseQualityBy05Days() {
        GildedRose products = createGildedRose(GildedRoseFactory.BACKSTAGE_PASSES_ITEM, 5, 1);
        products.updateQuality();

        assertEquals(4, getItemQuality(products));
    }


    @Test
    public void backstageEntryIncreaseQualityBy00Days() {
        GildedRose products = createGildedRose(GildedRoseFactory.BACKSTAGE_PASSES_ITEM, 0, 1);
        products.updateQuality();

        assertEquals(0, getItemQuality(products));
    }


    @Test
    public void sulfurasInmutableLegendaryQuality() {
        GildedRose products = createGildedRose(GildedRoseFactory.SULFURAS, 1, 1);
        products.updateQuality();

        assertEquals(80, getItemQuality(products));
    }


    @Test
    public void sulfurasInmutableLegendarySellIn() {
        GildedRose products = createGildedRose(GildedRoseFactory.SULFURAS, 1, 1);
        products.updateQuality();

        assertEquals(1, getItemSellIn(products));
    }


    @Test
    public void conjuredDecreaseSellIn() {
        GildedRose products = createGildedRose(GildedRoseFactory.CONJURED_ITEM, 10, 10);
        products.updateQuality();

        assertEquals(9, getItemSellIn(products));
    }


    @Test
    public void conjuredIncreaseDoubleQuality() {
        GildedRose products = createGildedRose(GildedRoseFactory.CONJURED_ITEM, 10, 10);
        products.updateQuality();

        assertEquals(8, getItemQuality(products));
    }


    @Test
    public void standardDecreaseSellIn() {
        GildedRose products = createGildedRose("Standard", 10, 10);
        products.updateQuality();

        assertEquals(9, getItemSellIn(products));
    }


    @Test
    public void standardDecreaseQuality() {
        GildedRose products = createGildedRose("Standard", 10, 10);
        products.updateQuality();

        assertEquals(9, getItemQuality(products));
    }


    @Test
    public void standardDecreaseDoubleQuality() {
        GildedRose products = createGildedRose("Standard", 0, 10);
        products.updateQuality();

        assertEquals(8, getItemQuality(products));
    }


    private GildedRose createGildedRose(String name, int sellIn, int quality) {
        return new GildedRose(new Item[]{new Item(name, sellIn, quality)});
    }


    private int getItemSellIn(GildedRose products) { return products.items[0].sellIn; }


    private int getItemQuality(GildedRose products) {
        return products.items[0].quality;
    }
}
