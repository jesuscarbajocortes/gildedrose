package com.gildedrose;

public class GildedValues {

    private final static String SULFURAS = "Sulfuras, Hand of Ragnaros";
    private final static String AGED_BRIE = "Aged Brie";
    private final static String BACKSTAGE_ENTRY = "Backstage passes to a TAFKAL80ETC concert";
    private final static String CONJURED_ITEM = "Conjured";
    public static int MIN_QUANTITY_VALUE = 0;
    public static int MAX_QUALITY_VALUE = 50;
    public static int LEGENDARY_QUALITY_VALUE = 80;
    public static int NORMAL_QUALITY_VALUE = 1;
    public static int DOUBLE_QUALITY_VALUE = 2;
}
