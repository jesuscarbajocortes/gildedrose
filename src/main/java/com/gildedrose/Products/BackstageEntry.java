package com.gildedrose.Products;

import com.gildedrose.GildedValues;
import com.gildedrose.Item;
import com.gildedrose.ProductItemInterface;

public class BackstageEntry implements ProductItemInterface {

    private final Item item;

    public BackstageEntry(Item item) {
        this.item = item;
    }

    /**
     * "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
     * Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
     * Quality drops to 0 after the concert
     */
    @Override
    public void updateArticle() {

        increaseQualityByOne();

        if (sellInDateLessThan(11)) {
            increaseQualityByOne();
        }

        if (sellInDateLessThan(6)) {
            increaseQualityByOne();
        }

        if (sellInDateLessThan(1)) {
            fixQualityToMax(false);
        }

        manageMaxQuality();
        decreaseSellInByOne();
    }


    @Override
    public void decreaseSellInByOne() {
        item.sellIn--;
    }


    private boolean isLessQualityThanMax() {
        return item.quality < GildedValues.MAX_QUALITY_VALUE;
    }


    private void increaseQualityByOne() {
        if (isLessQualityThanMax()) {
            item.quality++;
        }
    }


    private boolean sellInDateLessThan(int days) {
        return item.sellIn < days;
    }


    private void fixQualityToMax(boolean isMax) {
        item.quality = (isMax ? GildedValues.MAX_QUALITY_VALUE : GildedValues.MIN_QUANTITY_VALUE);
    }


    private void manageMaxQuality() {
        if (!isLessQualityThanMax()) {
            fixQualityToMax(true);
        }
    }
}
