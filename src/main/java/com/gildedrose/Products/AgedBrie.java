package com.gildedrose.Products;

import com.gildedrose.GildedValues;
import com.gildedrose.Item;
import com.gildedrose.ProductItemInterface;

public class AgedBrie implements ProductItemInterface {

    private final Item item;

    public AgedBrie(Item item) {
        this.item = item;
    }

    /**
     * La calidad de un artículo nunca es negativa
     * El "Queso Brie envejecido" (Aged brie) incrementa su calidad a medida que se pone viejo
     * <p>
     * Su calidad aumenta en 1 unidad cada día
     * luego de la fecha de venta su calidad aumenta 2 unidades por día
     */
    @Override
    public void updateArticle() {

        decreaseSellInByOne();

        increaseQualityByOne();

        if (hasDaysAvailableToSell()) {
            increaseQualityByOne();
        }

        controlMaxQuality();
    }

    @Override
    public void decreaseSellInByOne() {
        item.sellIn--;
    }


    private boolean isLessQualityThanMax() {
        return item.quality <= GildedValues.MAX_QUALITY_VALUE;
    }


    private boolean hasDaysAvailableToSell() {
        return item.sellIn < GildedValues.MIN_QUANTITY_VALUE;
    }


    private void increaseQualityByOne() {
        if (isLessQualityThanMax()) {
            item.quality++;
        }
    }


    private void controlMaxQuality() {

        if (item.quality > GildedValues.MAX_QUALITY_VALUE) {
            item.quality = GildedValues.MAX_QUALITY_VALUE;
        }
    }
}
