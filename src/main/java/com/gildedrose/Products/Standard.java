package com.gildedrose.Products;

import com.gildedrose.GildedValues;
import com.gildedrose.Item;
import com.gildedrose.ProductItemInterface;

public class Standard implements ProductItemInterface {

    private final Item item;

    public Standard(Item item) {
        this.item = item;
    }

    /**
     * Una vez que ha pasado la fecha recomendada de venta, la calidad se degrada al doble de velocidad
     * La calidad de un artículo nunca es negativa
     */
    @Override
    public void updateArticle() {

        decreaseQualityBy((item.sellIn > 0) ? GildedValues.NORMAL_QUALITY_VALUE : GildedValues.DOUBLE_QUALITY_VALUE);

        decreaseSellInByOne();
    }

    @Override
    public void decreaseSellInByOne() {
        item.sellIn--;
    }


    private void decreaseQualityBy(int value) {
        item.quality -= value;
        controlMinQuality();
    }


    private void controlMinQuality() {

        if (item.quality < GildedValues.MIN_QUANTITY_VALUE) {
            item.quality = GildedValues.MIN_QUANTITY_VALUE;
        }
    }
}
