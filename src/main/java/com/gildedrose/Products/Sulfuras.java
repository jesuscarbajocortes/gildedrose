package com.gildedrose.Products;

import com.gildedrose.GildedValues;
import com.gildedrose.Item;
import com.gildedrose.ProductItemInterface;

public class Sulfuras implements ProductItemInterface {

    private final Item item;

    public Sulfuras(Item item) {
        this.item = item;
    }

    /**
     * "Sulfuras" is a legendary item and as such its Quality is 80 and it never alters. I fix the value to prevent an incorrect value input.
     */
    @Override
    public void updateArticle() {

        item.quality = GildedValues.LEGENDARY_QUALITY_VALUE;
    }

    @Override
    public void decreaseSellInByOne() {
        item.sellIn--;
    }
}
