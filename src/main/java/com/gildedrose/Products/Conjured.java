package com.gildedrose.Products;

import com.gildedrose.GildedValues;
import com.gildedrose.Item;
import com.gildedrose.ProductItemInterface;

public class Conjured implements ProductItemInterface {

    private final Item item;

    public Conjured(Item item) {
        this.item = item;
    }

    /**
     * El artículo "Sulfuras" (Sulfuras), siendo un artículo legendario, no modifica su fecha de venta ni se degrada en calidad
     */
    @Override
    public void updateArticle() {

        decreaseSellInByOne();
        decreaseQualityBy(GildedValues.DOUBLE_QUALITY_VALUE);
    }


    @Override
    public void decreaseSellInByOne() {
        item.sellIn--;
    }


    private void decreaseQualityBy(int value) {
        item.quality -= value;
        controlMinQuality();
    }


    private void controlMinQuality() {

        if (item.quality < GildedValues.MIN_QUANTITY_VALUE) {
            item.quality = GildedValues.MIN_QUANTITY_VALUE;
        }
    }
}
