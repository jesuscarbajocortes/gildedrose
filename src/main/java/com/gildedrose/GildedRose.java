package com.gildedrose;

class GildedRose {

    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            manageProductFeatures(item);
        }
    }


    private void manageProductFeatures(Item item) {
        new GildedRoseFactory(item).getProduct(item).updateArticle();
    }
}