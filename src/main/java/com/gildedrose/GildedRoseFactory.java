package com.gildedrose;

import com.gildedrose.Products.*;

import java.util.HashMap;
import java.util.Map;

public class GildedRoseFactory {

    public final static String BRIE = "Aged Brie";
    public final static String BACKSTAGE_PASSES_ITEM = "Backstage passes to a TAFKAL80ETC concert";
    public final static String SULFURAS = "Sulfuras, Hand of Ragnaros";
    public final static String CONJURED_ITEM = "Conjured Mana Cake";
    private final static Map<String, ProductItemInterface> LIST_ITEMS = new HashMap<>();

    public GildedRoseFactory(Item item) {
        LIST_ITEMS.put(BRIE, new AgedBrie(item));
        LIST_ITEMS.put(BACKSTAGE_PASSES_ITEM, new BackstageEntry(item));
        LIST_ITEMS.put(SULFURAS, new Sulfuras(item));
        LIST_ITEMS.put(CONJURED_ITEM, new Conjured(item));
    }


    /**
     * @param item
     * @return
     */
    public ProductItemInterface getProduct(Item item) {

        return (LIST_ITEMS.containsKey(item.name)) ? LIST_ITEMS.get(item.name) : new Standard(item);
    }
}
