package com.gildedrose;

public interface ProductItemInterface {

    void updateArticle();

    void decreaseSellInByOne();

}
